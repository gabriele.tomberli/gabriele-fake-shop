
[![Netlify Status](https://api.netlify.com/api/v1/badges/47a642a6-9ffe-49d5-abe0-61bbe2efcf5e/deploy-status)](https://app.netlify.com/sites/gabriele-fake-shop/deploys)

# Gabriele's Fake Shop

This prototype demonstrates a basic E-Commerce website, with a simple product list and product details views.

The specification of the exercise can be found [here](https://gitlab.com/gabriele.tomberli/gabriele-fake-shop/-/blob/develop/docs/specs.pdf).

In my implementation, the landing page is a list of all the available products in the catalog. From there, an user can browse a specific category of products, or view the details of a single product. In the future the landing page could be extended to show new products, current offers or other information related to the current user (recently viewed, recommendations, etc).

My main objectives with this project were to:
- Create re-usable components, to be used easily in more complex applications
- Create a responsive website, usable on desktop computers, laptops, tablets and smartphones
- Provide a positive and intuitive user experience
- Minimize the use of external dependencies

It is powered by [Vite](https://vitejs.dev/), with [Vue](https://v3.vuejs.org/) and [Vue Router](https://next.router.vuejs.org/) for the presentation and [TailwindCSS](https://tailwindcss.com/) for the styling. All data is provided by [Fake Store API](https://fakestoreapi.com/).

A live version of this application is available at: [https://gabriele-fake-shop.netlify.app](https://gabriele-fake-shop.netlify.app/).

## How to run

To start the application in development mode, install the required dependencies via `npm install` and then start the development server via `npm start`. By default the server will start on port 3000.

To build the application, run `npm run build`. The resulting distribution will be available in the `dist` folder.

## Known issues

- When navigating between pages or reloading, product sorting resets to default. The best approach would probably be to save it in a Vuex store or in the URL.
