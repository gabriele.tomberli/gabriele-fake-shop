
/**
 * Array of default products sorting options, to be used in product list pages for a consistent set of options.
 * @type SortOption[]
 */
const productsSortOptions = [
	{ name: 'Name, A-Z', field: 'title', direction: 1 },
	{ name: 'Name, Z-A', field: 'title', direction: -1 },
	{ name: 'Price, ascending', field: 'price', direction: 1 },
	{ name: 'Price, descending', field: 'price', direction: -1 },
];
export default productsSortOptions;
