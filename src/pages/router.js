
import { createRouter, createWebHistory } from 'vue-router';

const LandingPage = () => import( './LandingPage.vue' );
const CategoryPage = () => import( './CategoryPage.vue' );
const ProductPage = () => import( './ProductPage.vue' );
const NotFoundPage = () => import( './NotFoundPage.vue' );

const routes = [
	{ path: '/', component: LandingPage },
	{ path: '/categories/:categorySlug', component: CategoryPage },
	{ path: '/products/:productId', component: ProductPage },
	{ path: '/:pathMatch(.*)*', name: 'NotFound', component: NotFoundPage },
];

const router = createRouter( {
	history: createWebHistory(),
	routes,
	linkActiveClass: 'active',
	linkExactActiveClass: 'active exact',
	scrollBehavior: function(to, from, savedPosition) {
		if (savedPosition) {
			// When using back / forward, reuse the previous position.
			return savedPosition
		} else {
			// Otherwise, scroll to top.
			return { x: 0, y: 0 }
		}
	}
} );

export default router;
