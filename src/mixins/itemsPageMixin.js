
/**
 * This mixin rapresent a basic component that is responsible of filtering, sorting and paginating a given set of items.
 * Fetching the initial set of all the items and placing them inside the items field is the only required responsability of the subcomponent.
 * All the other basic operations can be extended in sub components but are provided for simple use cases.
 * A requisite of this component is to be reusable more times on the same page, so no global state is ever read or written from this mixin (eg, from the URL).
 * 
 * It is used in this application to prevent duplication of basic listing functionalitis in LandingPage and CategoryPage, 
 * which offer a similar view of Products but with slightly different interfaces and functionalities.
 */
export default {
	data() {
		return {
			loading: false,
			items: [], // the list of all available items in the database
			sortOptions: [], // the list of possible sorting modes (SortOption[])
			sort: null,
			currentPage: 0,
			itemsPerPage: 4,
		}
	},
	created() {
		this.sort = this.defaultSortOption;
	},
	methods: {

		/**
		 * Template method used to sort the list of items. 
		 * A basic implementation based on native JavaScript comparison with the specified *sortField* is provided for the basic use cases.
		 * @param a {object} - the first item
		 * @param b {object} - the second item
		 * @returns -1 if a should be sorted before b, 1 otherwise
		 */
		compareItems( a, b ) {
			if (!this.sort)
				return -1;
			return ( a[this.sort.field] < b[this.sort.field] ) ? -1 : 1;
		},

		/**
		 * Template method used to filter the list of items. 
		 * In the basic implementation, no filtering is done.
		 * @param item {object} - the item to match
		 * @returns {boolean} true if item is a valid item with the current filters, false otherwise.
		 */
		matchItem( item ) { // eslint-disable-line no-unused-vars
			return true;
		}
		
	},
	computed: {
		totalPages() {
			return Math.ceil( this.items.length / this.itemsPerPage );
		},

		/**
		 * @returns {object[]} the list of all items that matches the current set of filters, already sorted and paginated.
		 */ 
		pageItems() {
			const startIndex = Math.max( 0, this.currentPage * this.itemsPerPage );
			const endIndex = Math.min( this.items.length, ( this.currentPage + 1 ) * this.itemsPerPage );
			const sortDirection = this.sort?.direction ?? 1;
			return this.items
				.filter( item => this.matchItem( item ) )
				.sort( (a, b) => this.compareItems( a, b ) * sortDirection )
				.slice( startIndex, endIndex );
		},
		
		/**
		 * @returns {SortOption} The default sorting option to use.
		 */ 
		defaultSortOption() {
			// By default, use the first option. 
			// Could be more complicated in the future, for example by having a default flag inside the array 
			// or using a store to persist the user choice between pages.
			return this.sortOptions[0]; 
		}
	}
};
