
import axios from 'axios';

/**
 * An utility class that contains most commonly used funcionalities of REST HTTP requests.
 * It is used as a middleware in the application to allow changing the underlying implementation without any implications for the rest of the application.
 * It could be moved to a shared library in the future to provide this functionality to multiple projects.
 */
class RestService {

	/**
	 * Executes a GET request to the given URL. Returns JSON data if successful, throws an error otherwise.
	 * @param url [string] - the url to retrieve.
	 * @param params {object} - a dictionary of get parameters to send.
	 * @returns Promise<any> - a promise containing the JSON data retrieved from the server, if the request to the server is succesful; a failed promise otherwise.
	 */
	async get(url, params = {}) {
		await new Promise(resolve => setTimeout(resolve, 2000));
		return axios.get( url, { params } ).then( response => response.data );
	}

	// TODO: Implement additional REST methods in case of need.

}

export default new RestService();
