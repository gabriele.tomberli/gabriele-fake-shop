
import restService from './rest-service';

const API_URL = 'https://fakestoreapi.com/products';

/**
 * Simple service that supplies products to the rest of the application by requesting data to fakestoreapi.com.
 */
class ProductsService {

	async getAllProducts() {
		return restService.get(API_URL);
	}

	async getProductsByCategory(categoryId) {
		return restService.get(`${ API_URL }/category/${ categoryId }`)
	}
	
	async getProductById(productId) {
		return restService.get(`${ API_URL }/${ productId }`)
	}

}

// Create the singleton instance of this service.
const productsService = new ProductsService();

export default productsService;
