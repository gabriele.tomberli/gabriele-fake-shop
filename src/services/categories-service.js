
// For now, categories are hard coded in this file.
// In the future, a different implementation could be used without changing the rest of the application.

const categories = [
	{
		id: "electronics",
		name: "Electronics",
		slug: "electronics",
	},
	{
		id: "jewelery",
		name: "Jewelery",
		slug: "jewelery",
	},
	{
		id: "men's clothing",
		name: "Men's Clothing",
		slug: "men-clothing",
	},
	{
		id: "women's clothing",
		name: "Women's Clothing",
		slug: "women-clothing",
	}
];

/**
 * Simple service that supplies product categories to the rest of the application.
 */
class CategoriesService {

	async getAllCategories() {
		return categories;
	}

}

// Create the singleton instance of this service.
const categoriesService = new CategoriesService();

export default categoriesService;
