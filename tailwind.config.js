module.exports = {
	mode: 'jit',
	purge: [ './index.html', './src/**/*.{vue,js}' ],
	important: true,
	theme: {
		extend: {},
		listStyleType: {
			none: 'none',
			disc: 'disc',
			square: 'square'
		}
	},
	variants: {
		extend: {},
	},
	plugins: [],
}
